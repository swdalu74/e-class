
# 1주차

## Subject

DFS / BFS / 다익스트라(Dijkstra)

## Exam

1. [물류창고](https://codepro.lge.com/exam/18/%EA%B5%AD%EB%82%B4-%EC%97%B0%EC%8A%B5%EB%AC%B8%EC%A0%9C/quiz/9)
2. [도로건설](https://codepro.lge.com/exam/18/%EA%B5%AD%EB%82%B4-%EC%97%B0%EC%8A%B5%EB%AC%B8%EC%A0%9C/quiz/5)
3. [디스플레이 필터](https://codepro.lge.com/exam/18/%EA%B5%AD%EB%82%B4-%EC%97%B0%EC%8A%B5%EB%AC%B8%EC%A0%9C/quiz/6)
4. [무인열차](https://codepro.lge.com/exam/18/%EA%B5%AD%EB%82%B4-%EC%97%B0%EC%8A%B5%EB%AC%B8%EC%A0%9C/quiz/8)

## Refreneces

>_학습에 도움되는 내용을 추가해 주세요._

- [다이나믹 프로그래밍(Dynamic Programming) : https://blog.naver.com/ndb796/221233570962](https://blog.naver.com/ndb796/221233570962)
- [깊이 우선 탐색(DFS) : https://blog.naver.com/ndb796/221230945092](https://blog.naver.com/ndb796/221230945092)
- [너비 우선 탐색(BFS) : https://blog.naver.com/ndb796/221230944971](https://blog.naver.com/ndb796/221230944971)
- [다익스트라(Dijkstra) : https://blog.naver.com/ndb796/221234424646](https://blog.naver.com/ndb796/221234424646)
- [합집합 찾기(Union-Find) : https://m.blog.naver.com/ndb796/221230967614](https://m.blog.naver.com/ndb796/221230967614)

## Notice

- 자유롭게 정보를 추가해 주세요.

## Wrap-up

- 1주차 완료 후 정리

#include <iostream>
#include <vector>
#include <cstdlib>
using namespace std;

int N, skipCnt, minCap = 1000000;
vector<pair<int, int>> filter;

void InputData()
{
	int r, p;
	cin >> N;
	for(int i=0; i<N; i++)
	{
		cin >> r >> p;
		filter.push_back({r, p});
	}

	return;
}

void DFS(int n, int reflect, int penetrate, int skip)
{
	int cap;

	if(n == N)
	{
		cap = abs(reflect - penetrate);
		if(minCap > cap || (minCap == cap && skip > skipCnt))
		{
			minCap = cap;
			skipCnt = skip;
		}
		return;
	}

	if(skip < (N-1))
	{
		skip++;
		DFS(n+1, reflect, penetrate, skip);
		skip--;
	}

	DFS(n+1, reflect * filter[n].first, penetrate + filter[n].second, skip);

	return;
}

void solve()
{
	DFS(0, 1, 0, 0);

	return;
}

int main() {
	InputData();

	solve();

	cout << skipCnt << endl;

	return 0;
}
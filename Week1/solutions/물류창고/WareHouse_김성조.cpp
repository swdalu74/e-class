#include <bits/stdc++.h>
using namespace std;

typedef pair<int, int> pii;
const int INF = int(1e9);
int V, E, start, dist[110];
vector<pii> adj[110];
int max_list[110];

int main() {
	cin >> V >> E;

	for (int i = 0; i < E; i++) {
		int u, v, w; cin >> u >> v >> w;
		adj[u].push_back({ w, v });
		adj[v].push_back({ w, u });
	}

	for (int i = 1; i <= V; i++)
	{
		fill(dist + 1, dist + V + 1, INF);
		priority_queue<pii, vector<pii>, greater<pii>> pq;
		dist[i] = 0;
		pq.push({ dist[i], i });
#if 1
		while (!pq.empty())
		{
			auto cur = pq.top(); pq.pop();
			int cost = cur.first, idx = cur.second;

			if (dist[idx] != cost) continue;

			for (auto next : adj[idx])
			{
				int ncost = next.first, nidx = next.second;
				if (dist[nidx] > cost + ncost)
				{
					dist[nidx] = cost + ncost;
					pq.push({ dist[nidx], nidx });
				}
			}
		}
#else
		while (!pq.empty())
		{
			auto [cost, idx] = pq.top(); pq.pop();		

			if (dist[idx] != cost) continue;

			for (auto [ncost, nidx] : adj[idx])
			{				
				if (dist[nidx] > cost + ncost)
				{
					dist[nidx] = cost + ncost;
					pq.push({ dist[nidx], nidx });
				}
			}
		}
#endif
		int max_dist = 0;
		for (int j = 1; j <= V; j++)
		{
			if (dist[j] > max_dist)
				max_dist = dist[j];
		}
		max_list[i] = max_dist;

		//cout << "CHECK " << i << " " << max_dist << '\n';
	}

	int min_result = INF;
	for (int i = 1; i <= V; i++)
	{
		if (max_list[i] < min_result)
			min_result = max_list[i];
	}

	cout << min_result;

	return 0;
}

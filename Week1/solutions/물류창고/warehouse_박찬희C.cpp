#include <iostream>
#include <queue>
#include <vector>
#include <string.h>

using namespace std;

int N;
int M;

#define MAXN 110
int INF = 1000000000;

vector<pair<int, int>> a[MAXN];

int d[MAXN];
int d1[MAXN][MAXN];
int output[MAXN];

void InputData(){
	int c, n, d;

	cin >>N>>M;

	for (int i = 1; i <= M; i++){
		cin>>c>>n>>d;
		a[c].push_back(make_pair(n, d));
		a[n].push_back(make_pair(c, d));
	}
}

void sol(int start) {
	priority_queue<pair<int, int>> pq;

	d[start] = 0;
	pq.push(make_pair(start, d[start]));

	while(!pq.empty()){
		int current = pq.top().first;
		int distance = -pq.top().second;

		pq.pop();

		if (d[current] < distance) continue;

		for (int i = 0; i < a[current].size(); i++){
			int next = a[current][i].first; 
			int nextDistance = a[current][i].second; 
			if (distance + nextDistance < d[next]){
				d[next] = distance + nextDistance;

				pq.push(make_pair(next, -d[next]));
			}
		}
	}
}

int main() {
	InputData();

	for (int i = 0; i < MAXN ; i++){
		d[i] = INF;
	}

	for (int i = 1; i <=N ;i++){
		sol(i);
		memcpy(d1[i], d, sizeof(int)*MAXN);
 
		for (int j = 0; j < MAXN; j++){
			d[j] = INF;
		}
	}

	int max = 0;
	for (int i = 1; i<= N; i++){
		max = 0;
		for (int j = 1; j <= N; j++){
			if (max < d1[i][j]) {
				max = d1[i][j];
			}
		}
		output[i] = max;
	}

	int min = INF;
	for (int i = 1; i <= N; i++){
		if (min > output[i]){
			min = output[i];
		}
	}
	cout<<min<<endl;

	return 0;
}

#include <iostream>
#include <queue>
#include <vector>

#define INFDIST 0xFF

using namespace std;

using DistancesData = vector<int>;
using PairData = pair<int, int>;
using FactoryData = vector<PairData>;

void InputData(vector<FactoryData> &factoryData,
               vector<DistancesData> &distData) {
  int numFactory;
  int numRoad;
  cin >> numFactory;
  cin >> numRoad;
  for (int i = 0; i < numFactory; i++) {
    factoryData.push_back(FactoryData());
    distData.push_back(DistancesData(numFactory, INFDIST));
  }

  for (int i = 0; i < numRoad; i++) {
    int f1, f2, dist;
    cin >> f1 >> f2 >> dist;
    if (f1 != f2 && dist > 0 && dist <= 100) {
      factoryData[f1 - 1].push_back(make_pair(f2 - 1, dist));
      factoryData[f2 - 1].push_back(make_pair(f1 - 1, dist));
    }
  }
}

void dijkstra(int startFactoryNum, vector<FactoryData> &factoryData,
              DistancesData &distData) {
  priority_queue<PairData, vector<PairData>, greater<PairData>> pq;
  distData[startFactoryNum] = 0;
  pq.push(make_pair(startFactoryNum, 0));

  while (!pq.empty()) {
    int current = pq.top().first;
    int distance = pq.top().second;

    pq.pop();

    if (distData[current] < distance)
      continue;

    for (int i = 0; i < factoryData[current].size(); i++) {
      int next = factoryData[current][i].first;
      int nextDist = distance + factoryData[current][i].second;

      if (nextDist < distData[next]) {
        distData[next] = nextDist;
        pq.push(make_pair(next, nextDist));
      }
    }
  }
}

int main() {
  vector<FactoryData> factories;
  vector<DistancesData> distances;
  InputData(factories, distances);
  int minStoreDist = INFDIST;
  for (int j = 0; j < factories.size(); j++) {
    int maxDist = 0;
    dijkstra(j, factories, distances[j]);
    for (int i = 0; i < distances[j].size(); i++) {
      if (maxDist < distances[j][i])
        maxDist = distances[j][i];
    }

    if (minStoreDist > maxDist)
      minStoreDist = maxDist;
  }
  cout << minStoreDist << endl;
  return 0;
}
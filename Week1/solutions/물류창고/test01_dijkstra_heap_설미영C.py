import heapq
import sys
input = sys.stdin.readline

INF =  int(1e9)
n,m =  map(int, input().split())
start = 0

g = [[] for i in range(n + 1)]


for _ in range(m):
    a,b,c = map(int, input().split())
    g[a].append((b,c)) #튜플형태로 a에서 b까지 가는 거리는 c
    g[b].append((a,c))

max_distance =[] #가장 먼 거리 들중 가장 작은 값을 구하기 위해 
#start는 노드 갯수만큼이 될수 있다
def dijkstra(start):
    distance = [INF] * (n + 1)
    q=[]
    heapq.heappush(q, (0,start))
    distance[start] = 0

    while q:
        dist, now = heapq.heappop(q)
        if distance[now] < dist:
            continue
        for i in g[now]:
            cost = dist + i[1] #튜플로 저장했으므로 (b,c)즉 인덱스 1번이 거리
            if cost < distance[i[0]]: #저장되어있는 거리값이 거쳐가는 거리값보다 크다면
                distance[i[0]] = cost #더 작은 값을 distance 테이블에 넣어 줌
                heapq.heappush(q,(cost,i[0]))
    distance.sort()
    # print("distance : ", distance)
    max_distance.append(distance[-2])
    return max_distance

for i in range(1, n + 1):
    dijkstra(i)
max_distance.sort()
print(max_distance[0])

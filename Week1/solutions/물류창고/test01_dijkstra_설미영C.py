import sys
from collections import deque

input = sys.stdin.readline
n, m = map(int, input().split())
INF = int(1e9) #무한
start = 0
#각 노드에 인접한 노드정보를 담는 2차원 리스트 (모든노드에서 모든노드이므로)
g = [[] for i in range(n + 1)]
#모든 간선 정보 입력받기
for _ in range(m):
    a,b,c = map(int, input().split())
    g[a].append((b,c)) #a에서 b까지 가는 거리가 c
    g[b].append((a,c)) #양방향이므로 b에서 a까지 가는 거리도 c

max_distance =[]
for x in range(1, n + 1):
    start = x
    #방문 여부 체크 리스트
    visited = [False] * (n + 1)
    #최단 거리 테이블을 무한으로 초기화
    distance = [INF] * (n + 1)

    #방문하지 않은 노드 중에서, 가장 거리가 짧은 노드 번호를 반환
    def get_smallest_node():
        min_value = INF
        index = 0 #가장 거리가 짧은 노드
        for i in range(1, n + 1):
            if distance[i] < min_value and not visited[i]:
                min_value = distance[i]
                index = i
        return index

    def dijkstra():
            distance[start] = 0 #시작노드의 거리 초기화(자기자신에서 자기자신으로 가는것이므로 0)
            visited[start] = True #시작 노드 방문처리

            for j in g[start]: #시작노드와 인접노드를 차례로 확인
                distance[j[0]] = j[1] #주어진 인접노드의 비용(=거리)을 distance[노드]에 삽입

            for i in range(n - 1) : #시작 노드를 제외한 n-1개의 노드에 대해 반복
                now = get_smallest_node() #거리가 가장짧은 노드를 꺼내서 방문처리
                visited[now] = True
                for j in g[now]: #현재 노드와 연결된 인접노드를 차례로 확인
                    cost = distance[now] + j[1] #비용(=총거리)는  start 노드에서 now 노드의까지 거리 + now노드에서 end노드까지의 거리
                    if cost < distance[j[0]]: #now노드를 거쳐서 end노드로 이동하는 거리가 더 짧은 경우
                        distance[j[0]] = cost #거리 테이블을 더 짧은 cost값으로 갱신

            distance.sort()
            max_distance.append(distance[-2])
            max_distance.sort()
            return max_distance

    dijkstra()
print(max_distance[0])

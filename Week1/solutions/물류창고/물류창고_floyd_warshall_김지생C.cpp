#include <iostream>
using namespace std;

int A[100+10], B[100+10], D[100+10];
int dist[100+10][100+10];
int N, M;

void input(void)
{
	int i, s, e;

	cin >> N >> M;

	for(i=0;i<M;i++)
	{
		cin >> A[i] >> B[i] >> D[i];
	}

#if 0
	for(i=0;i<M;i++)
			printf("%d %d %d\n", A[i],B[i],D[i]);
#endif

	for (s=1;s<=N;s++)
	{
		for(e=1;e<=N;e++)
		{
			if(s!=e)
				dist[s][e] = 1000;
			else
				dist[s][e] = 0;
		}
	}

	for (i=0 ; i<M ; i++)
	{
			dist[A[i]][B[i]] = dist[B[i]][A[i]] = D[i];
	}

}

int solve(void)
{
	int k, s, e;
	int max,ans;
	for(k = 1;k<=N;k++)
	{
		for(s = 1;s<=N;s++)
		{
			for(e=1;e<=N;e++)
			{
				if(dist[s][e]>dist[s][k]+dist[k][e])
				{
					dist[s][e]=dist[s][k]+dist[k][e];
				}
			}
		}
	}

	ans = 10000;
    for(s=1;s<=N;s++)
    {
        max = 0;
        for(e=1;e<=N;e++)
        {
            if(max<dist[s][e])
                max = dist[s][e];
        }
        if(ans>max) ans = max;
    }

	return ans;
}

void output(int max)
{
	//printf("%d\n", max);
	cout << max;

}

int main() {

	int ans;
	input();

	ans = solve();

	output(ans);

	return 0;
}

#include <iostream>
using namespace std;

#define IMP 100000;

int N, M;
int wp,rp;
int A[100*100], B[100*100], D[100*100];
int visit[100*100],dist[100][100];
int queue[100*100*100];

void inputdata()
{
	int i;
	cin >> N >> M;
	for(i=1 ; i<=M ; i++)
		cin >> A[i] >> B[i] >> D[i];

}

void outputdata(int ans)
{
	cout << ans <<endl;
}


void push(int s, int t)
{
	//현재 start 에서 destination node까지의 거리(visit[s])와
	//현재 check하고 있는 node까지의 거리 + 현재 check하는 node에서 destination까지의 거리(t)
	//visit[s] 와 t를 비교해서 더 좋은 것으로 갱신하고 좋을때의 node 값을 큐에 저장
	if(visit[s]<=t) return;
	visit[s] = t;
	queue[wp++] = s;
}

int pop()
{
	return queue[rp++];
}

int BFS(int s)  // 하나의 노드에서 최대거리 구하기
{
	int i, j, k;
	int max = 0;
	int current;
	wp = rp = 0;

// Visit 	함수 IMP로 초기화
	for (i = 1 ; i<=N ; i++)
		visit[i] = IMP;

	push(s,0);

	//s node에서 visit 배열 경신

	while(wp!=rp)
	{
		current = pop();
		for(j=1 ; j<=N ; j++){
			push(j, visit[current]+dist[current][j]);
		}
	}

	for(k=1 ; k<=N ; k++)
	{
		if(max < visit[k])
			max = visit[k];
	}

	return max;
}

int solve()
{
	int min = IMP;
	int ret,i,j;

// 현재 data로 표 작성

	for(i=1;i<=N;i++)
	{
		for(j=1;j<=N;j++)
			dist[i][j] = IMP;
	}

	for(i=1;i<=M;i++)
	{
		dist[A[i]][B[i]] = dist[B[i]][A[i]] = D[i];
	}

	for(i=1 ; i<=N ; i++)
	{
		ret = BFS(i);
		if(ret < min)
			min = ret;
	}

	return min;
}


int main()
{
	int ans = -1;
	inputdata();
	ans = solve();
	outputdata(ans);

}
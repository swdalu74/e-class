import sys
input = sys.stdin.readline

INF = int(1e9) #무한. 도달불가능일때
#노드(물류창고)갯수, 간선(도로)갯수 입력받기
n, m = map(int, input().split())

#2차원 리스트를 생성해서 초기값을 무한으로 초기화
g = [[INF] * (n + 1) for _ in range(n + 1)]

#자기 자신에서 자기자신으로 가는 거리는 0으로 초기화
for a in range(1, n + 1):
    for b in range(1, n +1):
        if a == b:
            g[a][b] = g[b][a] = 0 #양방향이므로

#각 간선에 대한 정보를 입력 받아, 그 값으로 초기화
for _ in range(m):
    #A에서 B로 가는 거리를 C라고 함. 양방향이므로 B에서 A로 가는 거리도 C
    a,b,c = map(int, input().split())
    g[a][b] = g[b][a] = c

#점화식에 따라 플로이드 워셜 알고리즘 수행
for k in range(1, n + 1):
    for a in range(1, n + 1):
        for b in range(1, n + 1):
            g[a][b] = min(g[a][b], g[a][k]+g[k][b])

#수행된 결과 출력
max_distance = [] #각 노드별로 가장 먼 거리를 저장
for a in range(1, n + 1):
    distance = []
    for b in range(1, n + 1):
        #도달 할 수 있는 경우
        if g[a][b] != INF:
            distance.append(g[a][b])
            distance.sort()
        # else: #도달할 수 없는 경우
        #     pass
    max_distance.append(distance[-1])
max_distance.sort() #가장먼 거리들을 오름차순으로 정렬해서 가장 작은 값을 출력
print(max_distance[0])

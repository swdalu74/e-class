#include <iostream>
#include <queue>
using namespace std;

#define MAXN (100)
int M, N;
int map[MAXN+10][MAXN+10];
int dist[MAXN+10];

void InputData()
{
    int s1, s2, d;

    cin >> N >> M;
    for(int i=1; i<=M; i++)
    {
        cin >> s1 >> s2 >> d;
        map[s1][s2] = d;
        map[s2][s1] = d;
    }
}

int BFS(int s)
{
    int node, maxD = 0, target = 0;
    queue<int> que;

    for(int i=1; i<=N; i++)
        dist[i] = 100 * MAXN + 10;

    que.push(s);
    dist[s] = 0;

    while(!que.empty())
    {
        node = que.front();
        que.pop();

        for(int i=1; i<=N; i++)
        {
            target = dist[node] + map[node][i];
            if(dist[i] > target && map[node][i] != 0)
            {
                que.push(i);
                dist[i] = target;
            }
        }
    }

    for(int i=1; i<=N; i++)
    {
        if(maxD < dist[i])
        maxD = dist[i];
    }

    return maxD;
}

void solve()
{
    int ret, minD = 100 * MAXN + 10;

    for(int i=1; i<=N; i++)
    {
        ret = BFS(i);

        if(minD > ret)
            minD = ret;
    }

    cout << minD << endl;

    return;
}

int main() {
    InputData();

    solve();

    return 0;
}

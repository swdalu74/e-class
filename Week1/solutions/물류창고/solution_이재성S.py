# N : 공장수
# M : 도로수
# d : 공장 사이 거리
N,M = list(map(int, input().split()))

d=list([] for _ in range(M))
for i in range(0,M):
    x,y,distance = list(map(int, input().split()))

    if x > y:
        FactoryFrom=y
        FactoryTo=x
    else:
        FactoryFrom=x
        FactoryTo=y

    d[i] = list([FactoryFrom,FactoryTo,distance])




#2차원 배열 생성(특정 공장에서 다른 공장까지의 거리를 나타낸 표)
resultdist=[[[] for _ in range(N)] for _ in range(N)]




#표 채우기
for i in range(1,N+1): #모든 공장들에 대해
    for x in d: #모든 도로에 대해
        if x[0]==i: # i번 공장에서 출발하는 도로이면
            #print("x[0]="+str(x[0])+" x[1]="+str(x[1])+" x[2]="+str(x[2]))
            resultdist[i-1][x[1]-1].append(x[2]) #도착지 x[2] 거리 추가

            # 기존 데이터에 i번 공장으로 도착하는 거리가 있으면 도착지 x[2] 거리 추가 (기존 -> i -> x[2])
            for y in resultdist:
                if len(y[i-1]) != 0:
                    for z in y[i-1]:
                        y[x[1]-1].append(z+x[2])




#resultdist[출발공장번호][도착공장번호]
#       도착공장1 2 3 4 5
#출발공장1 [[[], [5], [10, 19], [10, 25, 34], [18, 27, 25, 40, 49]],
#       2 [[], [], [14], [5, 29], [22, 20, 44]],
#       3 [[], [], [], [15], [8, 30]],
#       4 [[], [], [], [], [15]],
#       5 [[], [], [], [], []]]




#특정 공장 출발-특정 공장 도착의 가장 가까운 도로 거리 계산, 특정 공장 기준 가장 먼 거리 별도 저장(물류창고후보위치)
finalresult=[]
for list1 in resultdist:
    biggest = -1
    for list2 in list1:
        if len(list2) != 0:
            list2.sort()
            if list2[0]>biggest:
                biggest=list2[0]

    finalresult.append(biggest)




#가장 먼 공장거리가 가장 작은 물류창고 위치 찾기
minuscount=finalresult.count(-1)
for i in range(0,minuscount):
    finalresult.remove(-1)

finalresult.sort()
print(finalresult[0])
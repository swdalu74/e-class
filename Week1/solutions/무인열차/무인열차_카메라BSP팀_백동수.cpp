#include <iostream>
#include <map>
#include <vector>
#include <cstdlib>

#define MAXD 100
using namespace std;
using pii = pair<int, int>;

map<pii, char> m;
vector<pii> L1;
vector<pii> L2;

int W, H;
int dy[4] = {0, 0, -1, 1};
int dx[4] = {-1, 1, 0, 0};

void InputData()
{
    char rail;
    cin >> H >> W;

    for(int i=0; i<H; i++)
    {
        for(int j=0; j<W; j++)
        {
            cin >> rail;
            m[{i, j}] = rail;
        }
    }
}

void DFS(int y, int x, bool firstFind)
{
    int ymove, xmove;

    if(m[{y, x}] == '1')
    {
        if(firstFind == true)
        {
            L1.push_back({y, x});
        }
        else
        {
            L2.push_back({y, x});
        }

        m[{y, x}] = '0';
    }

    for(int i=0; i<4; i++)
    {
        ymove = y+dy[i];
        xmove = x+dx[i];

        if(ymove < 0 || ymove >= H || xmove < 0 || xmove >= W)
            continue;

        if(m[{ymove, xmove}] != '1')
            continue;

        DFS(ymove, xmove, firstFind);
    }
}

int solve()
{
    bool firstFind = true;
    int y, x, distance, minDist = MAXD;

    for(int i=0; i<H; i++)
    {
        for(int j=0; j<W; j++)
        {
            if(m[{i, j}] == '1')
            {
                DFS(i, j, firstFind);
                firstFind = false;
            }
        }
    }

    for(auto iter1=L1.begin(); iter1!=L1.end(); iter1++)
    {
        y = iter1->first;
        x = iter1->second;

        for(auto iter2=L2.begin(); iter2!=L2.end(); iter2++)
        {
            distance = abs(y - iter2->first) + abs(x - iter2->second);

            if(minDist > distance)
                minDist = distance;
        }
    }

    return minDist - 1;
}

int main() {
    int ans;

    InputData();

    ans = solve();

    cout << ans << endl;

    return 0;
}
#include <iostream>
#include <queue>
#include <vector>
#include <map>
using namespace std;

#define MAXN 100
int N;
map<pair<int, int>, int> m;
map<pair<int, int>, int> visit;

void InputData()
{
    int dist;
    cin >> N; 

    for(int i=0; i<N; i++)
    {
        for(int j=0; j<N; j++)
        {
            scanf("%1d",&dist);
            m[{j, i}] = dist;
        }
    }
    return;
}

void solve()
{
    int x, y, xmove, ymove, expense, newexp;
    queue<pair<int, int>> que;
    pair<int, int> pos;

    int dx[4] = {-1, 1, 0, 0};
    int dy[4] = {0, 0, -1, 1};

    que.push({0, 0});

    while(!que.empty())
    {
        pos = que.front();
        que.pop();
        auto [y, x] = pos;
        expense = visit[pos];

        for(int i=0; i<4; i++)
        {
            xmove = x+dx[i];
            ymove = y+dy[i];
            pos = {ymove, xmove};

            if(xmove < 0 || xmove >= N || ymove < 0 || ymove >= N)
                continue;

            newexp = expense + m[{ymove, xmove}];

            if(newexp < visit[pos] || visit[pos] == 0)
            {
                visit[pos] = newexp;
                que.push(make_pair(ymove, xmove));
            }
        }
    }

    return;
}

int main() {
    InputData();

    solve();

    cout << visit[{N-1, N-1}] << endl;

    return 0;
}

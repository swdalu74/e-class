#include <iostream>
#include <map>
#include <queue>

using namespace std;

using LandPositions = pair<int, int>;
using LandPrices = map<LandPositions, unsigned int>;

int InputData(vector<LandPositions> &lands, LandPrices &prices,
              LandPrices &accumlatedPrices) {
  int maxScale;
  cin >> maxScale;
  // cin >> numRoad;
  char lineStr[101] = {0};

  for (int i = 0; i < maxScale; i++) {
    cin >> lineStr;
    for (int j = 0; j < maxScale; j++) {
      auto coordinates = make_pair(i, j);
      prices[coordinates] = lineStr[j] - '0';
      accumlatedPrices[coordinates] = -1;
    }
  }
  return maxScale;
}

void BFS(int maxScale, LandPrices &prices, LandPrices &accumlatedPrices) {
  queue<LandPositions> landQ;
  auto startPos = make_pair(0, 0);
  accumlatedPrices[startPos] = 0;
  landQ.push(startPos);

  while (!landQ.empty()) {
    auto currentPos = landQ.front();
    auto currentPrice = accumlatedPrices[currentPos];
    landQ.pop();

    if (accumlatedPrices[currentPos] < currentPrice)
      continue;

    auto updateAccumlatedPrice = [currentPos, currentPrice, &landQ, &prices,
                                  &accumlatedPrices](LandPositions nextPos) {
      auto nextPrice = currentPrice + prices[nextPos];
      if (nextPrice < accumlatedPrices[nextPos]) {
        accumlatedPrices[nextPos] = nextPrice;
        landQ.push(nextPos);
      }
    };

    if (currentPos.first < maxScale - 1) {
      updateAccumlatedPrice(make_pair(currentPos.first + 1, currentPos.second));
    }
    if (currentPos.first > 0) {
      updateAccumlatedPrice(make_pair(currentPos.first - 1, currentPos.second));
    }
    if (currentPos.second > 0) {
      updateAccumlatedPrice(make_pair(currentPos.first, currentPos.second - 1));
    }
    if (currentPos.second < maxScale - 1) {
      updateAccumlatedPrice(make_pair(currentPos.first, currentPos.second + 1));
    }
  }
}

int main() {
  vector<LandPositions> landPos;
  LandPrices landPrices;
  LandPrices landAccumlatedPrices;
  int maxScale = InputData(landPos, landPrices, landAccumlatedPrices);
  BFS(maxScale, landPrices, landAccumlatedPrices);
  cout << landAccumlatedPrices[make_pair(maxScale - 1, maxScale - 1)] << endl;
  return 0;
}

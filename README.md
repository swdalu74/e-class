# e-Class

Sofware 역량인증 시험 스터디 그룹 e-Class

## Members

1. 설미영 책임연구원 (miyoung.seol@lge.com) [python]
2. 김성조 선임연구원 (seongjo.kim@lge.com) [c++]
3. 김지생 책임연구원 (jisaeng.kim@lge.com) [c]
4. 이재성 선임연구원 (jaesung.lee@lge.com) [python]
6. 백동수 선임연구원 (dongsu.bag@lge.com) [c++]
7. 박찬희 책임연구원 (chanhee.park@lge.com) [c++]
* Facilitator : 최인달 책임연구원 (indal.choi@lge.com)

## Schedule & Subjects

* _일정은 변경될 수 있습니다._

| | 날짜 | 주제 |
|:---:|---:|:---:|
| 1주차 | 2월 23일 | Orientation |
|       | 25일 | DFS/BFS/다익스트라(Dijkstra) |
| 2주차 | 3월 2일 |  |
|       | 4일 |  |
| 3주차 | 9일 |  |
|       | 11일 |  |
| 4주차 | 16일 |  |
|       | 18일 |  |
| 5주차 | 23일 |  |
|       | 25일 |  |

## 추천 Reference site

- [https://justicehui.github.io/navigator/](https://justicehui.github.io/navigator/) 사이트에서 검색을 활용하세요.

![](./etc/search_algorithm.gif) 

## Notice

- 소스코드는 주차별 디렉토리의 solutions 디렉토리에 올려주시고, 파일명은 이메일 아이디를 prefix로 달아 주세요. ex) Week1/solutions/indal.choi_week1_exam1.cpp

- e-Class 참여인원은 이 프로젝트에 속한 파일을 수정, 삭제하는 권한을 가지고 자유롭게 파일/디렉토리 생성이 가능합니다.

- 각 주차별 디렉토리의 sharing 디렉토리에 학습에 도움되는 자료를 자유롭게 올려주세요.

- 각 주차별 디렉토리의 README.md 파일에는 주별 선정된 주제에 대한 문제와 정보가 있습니다. 주제에 관련된 동영상 강의, 블로그등을 자유롭게 추가해 주세요.

- README.md 파일을 마크다운 문서입니다.

- 문서는 마크다운(MarkDown) 파일(확장자 .md)로 작성하는 것이 좋습니다.

- 마크다운 문서는 간결하고 작성법은 굉장히 쉽습니다.

- [MarkDown 사용법 1](https://heropy.blog/2017/09/30/markdown/)

- [MarkDown 사용법 2](https://gist.github.com/ihoneymon/652be052a0727ad59601)

- 파일 수정은 Web IDE를 사용하시면 편합니다.

![WebIDE](./etc/webIDE.PNG)
